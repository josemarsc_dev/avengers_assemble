package club.jsil.avengersassemble;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import vingador.Vingador;
import vingador.VingadorBD;

public class CadastrarVingador extends AppCompatActivity {

    private int vingador_id = 0;
    private EditText vingadorNomeEdit;
    private EditText vingadorHabilidadeEdit;
    private EditText vingadorStatusEdit;
    private EditText vingadorJuntouEdit;

    private Button salvar;
    private Button cancelar;

    private String nome;
    private String habilidade;
    private String status;
    private int juntou;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_vingador);

        salvar = findViewById(R.id.botaoSalvarVingador);
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VingadorBD vingadorBD = new VingadorBD(getBaseContext());
                Vingador vingador = new Vingador();

                vingadorNomeEdit = findViewById(R.id.vingadorCadastrarNome);
                vingadorHabilidadeEdit = findViewById(R.id.vingadorCadastrarHabilidade);
                vingadorStatusEdit = findViewById(R.id.vingadorCadastrarStatus);
                vingadorJuntouEdit = findViewById(R.id.vingadorCadastrarJuntou);

                nome = vingadorNomeEdit.getText().toString();
                habilidade = vingadorHabilidadeEdit.getText().toString();
                status = vingadorStatusEdit.getText().toString();
                juntou = Integer.parseInt(vingadorJuntouEdit.getText().toString());

                vingador.setNome(nome);
                vingador.setHabilidade(habilidade);
                vingador.setStatus(status);
                vingador.setJuntou(juntou);

                int result = -1;

                if(vingador_id == 0) {
                    result = vingadorBD.insert(vingador);
                } else {
                    vingador.set_id(vingador_id);
                    result = vingadorBD.update(vingador);
                }

                if(result == -1) {
                    Log.i("erro", "Erro ao salvar");
                    Toast.makeText(getApplicationContext(), "Erro ao salvar", Toast.LENGTH_LONG).show();
                } else {
                    Log.i("erro", "Salvo");
                    Toast.makeText(getApplicationContext(), "Salvo!", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });

        cancelar = findViewById(R.id.botaoCancelarVingador);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            vingador_id = Integer.parseInt(getIntent().getExtras().getString("vingador_id"));
            final VingadorBD vingadorBD = new VingadorBD(this);
            final Vingador vingador = vingadorBD.getById(vingador_id);

            vingadorNomeEdit = findViewById(R.id.vingadorCadastrarNome);
            vingadorHabilidadeEdit = findViewById(R.id.vingadorCadastrarHabilidade);
            vingadorStatusEdit = findViewById(R.id.vingadorCadastrarStatus);
            vingadorJuntouEdit = findViewById(R.id.vingadorCadastrarJuntou);

            vingadorNomeEdit.setText(vingador.getNome());
            vingadorHabilidadeEdit.setText(vingador.getHabilidade());
            vingadorStatusEdit.setText(vingador.getStatus());
            vingadorJuntouEdit.setText(Integer.toString(vingador.getJuntou()));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
