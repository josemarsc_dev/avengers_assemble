package club.jsil.avengersassemble;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import banco.Banco;
import usuario.Usuario;
import usuario.UsuarioBD;
import vingador.Vingador;
import vingador.VingadorBD;

public class MainActivity extends AppCompatActivity {

    private Button botaoRegistrar;
    private Button botaoLogin;

    EditText usernameEdit;
    EditText senhaEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeDb();

        botaoRegistrar = findViewById(R.id.botaoRegistrar);
        botaoRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CadastrarUsuario.class);
                startActivity(intent);
            }
        });

        usernameEdit = findViewById(R.id.username);
        senhaEdit = findViewById(R.id.senha);

        usernameEdit.setText("josemar");
        senhaEdit.setText("josemar");

        botaoLogin = findViewById(R.id.botaoLogar);
        botaoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UsuarioBD usuarioBD = new UsuarioBD(getBaseContext());

                String username = usernameEdit.getText().toString();
                String senha = senhaEdit.getText().toString();

                Usuario usuario = usuarioBD.getByUsuarioESenha(username, senha);

                if(usuario != null) {
                    Intent intent = new Intent(getApplicationContext(), ListarVingadores.class);
                    intent.putExtra("id_usuario", Integer.toString(usuario.get_id()));
                    startActivity(intent);
                    finish();
                } else {
                    Log.i("erro", "não foi possível fazer login");
                    Toast.makeText(getApplicationContext(), "não foi possível fazer login", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void initializeDb() {
        Banco banco = new Banco(this);
    }
}
