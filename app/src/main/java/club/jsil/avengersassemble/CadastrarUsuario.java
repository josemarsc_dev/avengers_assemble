package club.jsil.avengersassemble;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import usuario.Usuario;
import usuario.UsuarioBD;

public class CadastrarUsuario extends AppCompatActivity {

    private Button botaoCancelar;
    private Button botaoSalvar;

    private EditText nomeEdit;
    private EditText usernameEdit;
    private EditText senhaEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar_usuario);

        botaoCancelar = findViewById(R.id.botaoCancelarUsuario);
        botaoCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        botaoSalvar = findViewById(R.id.botaoSalvarUsuario);
        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UsuarioBD usuarioBD = new UsuarioBD(getBaseContext());

                nomeEdit = findViewById(R.id.nomeUsuario);
                usernameEdit = findViewById(R.id.usernameUsuario);
                senhaEdit = findViewById(R.id.senhaUsuario);

                String nome = nomeEdit.getText().toString();
                String username = usernameEdit.getText().toString();
                String senha = senhaEdit.getText().toString();

                Usuario usuario = new Usuario();
                usuario.setNome(nome);
                usuario.setUsername(username);
                usuario.setSenha(senha);

                int result;
                result = usuarioBD.insert(usuario);

                if(result == -1) {
                    Log.i("erro", "erro ao salvar usuario");
                    Toast.makeText(getApplicationContext(), "erro ao salvar usuario", Toast.LENGTH_LONG).show();
                } else {
                    Log.i("sucesso", "usuario salvo");
                    Toast.makeText(getApplicationContext(), "usuario salvo", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });
    }
}
