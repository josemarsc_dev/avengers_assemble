package club.jsil.avengersassemble;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import vingador.Vingador;
import vingador.VingadorAdapter;
import vingador.VingadorAsyncTask;
import vingador.VingadorBD;

public class ListarVingadores extends AppCompatActivity {

    private ListView listView;
    private FloatingActionButton fabCadastrarVingador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar_vingadores);

        fabCadastrarVingador = findViewById(R.id.cadastrarVingadorBotao);
        fabCadastrarVingador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CadastrarVingador.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

//        recuperarDoServidor();

        recuperarDoBancoLocal();
    }

    private void recuperarDoServidor() {
        try {
            String json = new VingadorAsyncTask().execute().get();
            ArrayList<Vingador> vingadores = parserJSON(this, json);

            VingadorAdapter pacienteAdapter = new VingadorAdapter(this, vingadores);
            ListView vingadoresListView = findViewById(R.id.listViewVingador);
            vingadoresListView.setAdapter(pacienteAdapter);

            listView = findViewById(R.id.listViewVingador);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), CadastrarVingador.class);
                    intent.putExtra("vingador_id", Long.toString(id));
                    startActivity(intent);
                }
            });
        } catch (Exception e) {
            Log.i("httpurlconnection", e.getMessage(), e);
        }
    }

    private void recuperarDoBancoLocal() {
        VingadorBD vingadorBD = new VingadorBD(getBaseContext());
        final Cursor cursor = vingadorBD.getAll();

        String[] fields = new String[]{"nome", "habilidade", "status", "juntou"};
        int[] idsViews = new int[]{
                R.id.vingadorNome,
                R.id.vingadorHabilidade,
                R.id.vingadorStatus,
                R.id.vingadorJuntou
        };

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                getBaseContext(),
                R.layout.vingador_view,
                cursor,
                fields,
                idsViews, 0);

        listView = findViewById(R.id.listViewVingador);
        listView.setAdapter(adapter);

        TextView vazio = findViewById(R.id.vazio);

        if (cursor.getCount() == 0) {
            vazio.setText("Nenhum registro encontrado");
            vazio.setVisibility(View.VISIBLE);
        } else {
            vazio.setText("");
            vazio.setVisibility(View.INVISIBLE);
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), CadastrarVingador.class);
                intent.putExtra("vingador_id", Long.toString(id));
                startActivity(intent);
            }
        });
    }

    private static ArrayList<Vingador> parserJSON(Context context, String json) throws IOException {
        ArrayList<Vingador> vingadores = new ArrayList<>();
        Vingador vingador;

        try {
            JSONObject root = new JSONObject(json);
            JSONArray jsonVingadores = root.getJSONArray("vingadores");

            for (int i = 0;i < jsonVingadores.length(); i++) {
                vingador = new Vingador();
                vingador.set_id(jsonVingadores.getJSONObject(i).getInt("id"));
                vingador.setNome(jsonVingadores.getJSONObject(i).getString("nome"));
                vingador.setHabilidade(jsonVingadores.getJSONObject(i).getString("habilidade"));
                vingador.setStatus(jsonVingadores.getJSONObject(i).getString("status"));
                vingador.setJuntou(jsonVingadores.getJSONObject(i).getInt("juntou"));

                Log.i("\n++++++++++++", "+++++++++++++");
                Log.i("id", Integer.toString(vingador.get_id()));
                Log.i("nome", vingador.getNome());
                Log.i("habilidade", vingador.getHabilidade());
                Log.i("status", vingador.getStatus());
                Log.i("juntou", Integer.toString(vingador.getJuntou()));
                Log.i("++++++++++++ ", "+++++++++++++");
                vingadores.add(vingador);
            }

        } catch (JSONException e) {
            Log.i("erro", e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
        return vingadores;
    }
}
