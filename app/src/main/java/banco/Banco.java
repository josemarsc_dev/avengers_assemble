package banco;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Banco extends SQLiteOpenHelper {

    public static final String NOME_BANCO = "vingadores.db";

    public Banco(Context context) {
        super(context, NOME_BANCO, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createUsuario = "CREATE TABLE usuario(" +
                "_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                "nome TEXT NOT NULL, " +
                "username TEXT NOT NULL UNIQUE," +
                "senha text NOT NULL)";
        String createVingadores = "CREATE TABLE vingadores(" +
                "_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "nome TEXT NOT NULL UNIQUE," +
                "habilidade TEXT NOT NULL," +
                "status TEXT NOT NULL," +
                "juntou INT NOT NULL)";

        try {
            db.execSQL(createUsuario);
            db.execSQL(createVingadores);
        } catch (SQLException ex) {
            ex.printStackTrace();
            Log.i("erro", ex.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS usuario");
        db.execSQL("DROP TABLE IF EXISTS vingadores");
        onCreate(db);
    }
}
