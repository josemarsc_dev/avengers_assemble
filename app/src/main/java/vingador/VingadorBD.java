package vingador;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import banco.Banco;

public class VingadorBD {
    private SQLiteDatabase bd;
    private Banco banco;

    public VingadorBD(Context context) {
        this.banco = new Banco(context);
    }

    public int insert(Vingador vingador) {
        ContentValues contentValues;

        int resultado;

        bd = banco.getWritableDatabase();
        contentValues = new ContentValues();
        contentValues.put("nome", vingador.getNome());
        contentValues.put("habilidade", vingador.getHabilidade());
        contentValues.put("status", vingador.getStatus());
        contentValues.put("juntou", vingador.getJuntou());

        String where = "_id = " + vingador.get_id();

        try {
            resultado = (int) bd.insert("vingadores", null, contentValues);
            bd.close();
        } catch (SQLiteException ex) {
            Log.i("Error", ex.getMessage());
            resultado = -1;
        }

        return resultado;
    }

    public int update(Vingador vingador) {
        ContentValues contentValues;

        int resultado;

        bd = banco.getWritableDatabase();
        contentValues = new ContentValues();
        contentValues.put("nome", vingador.getNome());
        contentValues.put("habilidade", vingador.getHabilidade());
        contentValues.put("status", vingador.getStatus());
        contentValues.put("juntou", vingador.getJuntou());

        String where = "_id = " + vingador.get_id();

        try {
            resultado = (int) bd.update(
                    "vingadores",
                    contentValues,
                    "_id = " + Integer.toString(vingador.get_id()),
                    null);
            bd.close();
        } catch (SQLiteException ex) {
            Log.i("Error", ex.getMessage());
            resultado = -1;
        }

        return resultado;
    }

    public Cursor getAll() {
        bd = banco.getReadableDatabase();
        Cursor cursor = null;
        cursor = bd.rawQuery("SELECT * FROM vingadores", null);

        return cursor;
    }

    public Vingador getById(int id) {
        bd = banco.getReadableDatabase();
        Vingador vingador = new Vingador();
        Cursor cursor = bd.rawQuery("SELECT * FROM vingadores WHERE _id=" + id, null);

        if(cursor.moveToFirst()) {
            vingador.set_id(cursor.getInt(cursor.getColumnIndex("_id")));
            vingador.setNome(cursor.getString(cursor.getColumnIndex("nome")));
            vingador.setHabilidade(cursor.getString(cursor.getColumnIndex("habilidade")));
            vingador.setStatus(cursor.getString(cursor.getColumnIndex("status")));
            vingador.setJuntou(Integer.parseInt(cursor.getString(cursor.getColumnIndex("juntou"))));
        }

        return vingador;
    }
}
