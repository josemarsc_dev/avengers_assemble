package vingador;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import club.jsil.avengersassemble.R;

public class VingadorAdapter extends ArrayAdapter<Vingador> {
    public VingadorAdapter(Context context, ArrayList<Vingador> vingadores) {
        super(context, 0, vingadores);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Vingador vingador= getItem(position);

        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.vingador_view, parent, false);
        }

        TextView vingadorNome = convertView.findViewById(R.id.vingadorNome);
        TextView vingadorHabilidade = convertView.findViewById(R.id.vingadorHabilidade);
        TextView vingadorStatus = convertView.findViewById(R.id.vingadorStatus);
        TextView vingadorJuntou = convertView.findViewById(R.id.vingadorJuntou);

        vingadorNome.setText(vingador.getNome());
        vingadorHabilidade.setText(vingador.getHabilidade());
        vingadorStatus.setText(vingador.getStatus());
        vingadorJuntou.setText(Integer.toString(vingador.getJuntou()));

        return convertView;
    }
}
