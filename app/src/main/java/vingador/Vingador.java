package vingador;

public class Vingador {
    private int _id;
    private String nome;
    private String habilidade;
    private String status;
    private int juntou;

    public Vingador() {

    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getHabilidade() {
        return habilidade;
    }

    public void setHabilidade(String habilidade) {
        this.habilidade = habilidade;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getJuntou() {
        return juntou;
    }

    public void setJuntou(int juntou) {
        this.juntou = juntou;
    }
}
