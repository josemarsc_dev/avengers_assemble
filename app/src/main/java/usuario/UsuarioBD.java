package usuario;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import banco.Banco;

public class UsuarioBD {

    private SQLiteDatabase bd;
    private Banco banco;

    public UsuarioBD(Context context) {
        this.banco = new Banco(context);
    }

    public int insert(Usuario usuario) {
        ContentValues contentValues;

        int resultado;

        bd = banco.getWritableDatabase();
        contentValues = new ContentValues();
        contentValues.put("nome", usuario.getNome());
        contentValues.put("username", usuario.getUsername());
        contentValues.put("senha", usuario.getSenha());

        String where = "_id = " + usuario.get_id();

        try {
            resultado = (int) bd.insert("usuario", null, contentValues);
            bd.close();
        } catch (SQLiteException ex) {
            Log.i("Error", ex.getMessage());
            resultado = -1;
        }

        return resultado;
    }

    public Usuario getByUsuarioESenha(String usuario, String senha) {
        bd = banco.getReadableDatabase();
        Usuario user = null;
        Cursor cursor = bd.rawQuery("SELECT * FROM usuario WHERE username = \"" + usuario + "\" AND senha = \"" + senha + "\"", null);

        if(cursor.moveToFirst()) {
            user = new Usuario();
            user.set_id(cursor.getInt(cursor.getColumnIndex("_id")));
            user.setNome(cursor.getString(cursor.getColumnIndex("nome")));
            user.setUsername(cursor.getString(cursor.getColumnIndex("username")));
        }

        return user;
    }

}
